#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdbool.h>
#include <semaphore.h>

int esperando = 0; //variable control para esperar
int cant_clientes = 3;
bool estado_barber;  // true--> cortando    // false----> durmiendo
int n=5; //sillas sala espera

sem_t a; //semaforo barbero
sem_t b; // semaforo cliente

void* barbero(void* arg){

	while (true) {
		if (esperando==0){			
			//dormir
			//estado_barber=false;
			printf("/esperando: %i", esperando);
			//printf("/estado barbero: %d", estado_barber);
			printf("	barbero: zzzzzzzzzz\n");
		}
		else{
			//cortar pelo
			//estado_barber=true;
			esperando--;			
			printf("/esperando: %i", esperando);
			printf("/estado barbero: %d", estado_barber);
			printf("	barbero: cortando\n");
			
			//cant_clientes--;
		}
		//sem_post(&b);
	}
}

void* cliente(void* arg){

	while (true) {
		if(esperando < n){
			
			//hay lugar, puedo entrar
			esperando++;
			
			if(estado_barber==true){
				//esperar en la silla
				printf("/esperando: %i", esperando);
				printf("/estado barbero: %d", estado_barber);
				printf("	cliente leyendo revista\n");
			}
			else if (estado_barber==false){
				//despertar al barber
				estado_barber=true;
				printf("/esperando: %i", esperando);
				printf("/estado barbero: %d", estado_barber);				
				printf("	barbero: uff, emmm, grrr,,,, uuuhh\n");
			}
		}

		else{
			//no hay lugar, me voy
			printf("/esperando: %i", esperando);
			printf("/estado barbero: %d", estado_barber);			
			printf("	cliente: chauuu nos vimoooooooooo $$$$$\n");
			
		}
	}
}
int main (void){

	sem_init(&a,0,1);
	sem_init(&b,0,0);
	pthread_t p1;

	pthread_t clientes_arr[cant_clientes];

	pthread_create(&p1,NULL,barbero,NULL);
	for (int i = 0; i<cant_clientes; i++){
		pthread_create(&clientes_arr[i],NULL,cliente,NULL);
		pthread_join (clientes_arr[i], NULL);
	}
	

	pthread_join (p1, NULL);
	printf("TERMINE");
	sem_destroy(&a);
	sem_destroy(&b);
	return 0;

}

