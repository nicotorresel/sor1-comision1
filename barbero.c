#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdbool.h>
#include <semaphore.h>

int cant_clientes;
bool estado_barber;  // true--> cortando    // false----> durmiendo
int n;  // sillas sala espera

sem_t a; //semaforo barbero
sem_t b; // semaforo cliente
pthread_mutex_t lock;

void* barbero(void* arg){
	
	sem_wait(&a);

	while (true) {
		pthread_mutex_lock(&lock);
		if (cant_clientes==0){
			//pthread_mutex_lock(&lock);
			//dormir
			estado_barber=false;
			printf("/cant client: %i", cant_clientes);
			printf("/estado barbero: %d", estado_barber);
			printf("	barbero: zzzzzzzzzz\n");
			//pthread_mutex_unlock(&lock);
		}
		else{
			//pthread_mutex_lock(&lock);
			//cortar pelo
			estado_barber=true;
			printf("/cant client: %i", cant_clientes);
			printf("/estado barbero: %d", estado_barber);
			printf("	barbero: cortando\n");
			cant_clientes--;
			//pthread_mutex_unlock(&lock);
		}
		sem_post(&b);
		pthread_mutex_unlock(&lock);
	}
}

void* cliente(void* arg){

	sem_wait(&b);
	while (true) {
		pthread_mutex_lock(&lock);
		if(cant_clientes < n){
			
			//hay lugar, puedo entrar
			cant_clientes++;
			//pthread_mutex_unlock(&lock);
			
		
			if(estado_barber==true){
				//pthread_mutex_lock(&lock);
				//esperar en la silla
				printf("/cant client: %i", cant_clientes);
				printf("/estado barbero: %d", estado_barber);
				printf("	cliente leyendo revista\n");
				//pthread_mutex_unlock(&lock);
				//sem_post(&a);
			}
			else if (estado_barber==false){
				//pthread_mutex_lock(&lock);
				//despertar al barber
				estado_barber=true;
				printf("/cant client: %i", cant_clientes);
				printf("/estado barbero: %d", estado_barber);				
				printf("	barbero: uff, emmm, grrr,,,, uuuhh\n");
				//pthread_mutex_unlock(&lock);
				//sem_post(&a);
			}
		}

		else{
			//pthread_mutex_lock(&lock);
			//no hay lugar, me voy
			printf("/cant client: %i", cant_clientes);
			printf("/estado barbero: %d", estado_barber);			
			printf("	cliente: chauuu nos vimoooooooooo $$$$$\n");
			
		}
	sem_post(&a);
	pthread_mutex_unlock(&lock);
	}
}
int main (void){

	sem_init(&a,0,1);
	sem_init(&b,0,0);
	cant_clientes = 0;
	n = 2;
	pthread_mutex_init(&lock, NULL);
	pthread_t p1;
	pthread_t p2;

	pthread_create(&p1,NULL,barbero,NULL);
	pthread_create(&p2,NULL,cliente,NULL);

	pthread_join (p1, NULL);
	pthread_join (p2, NULL);
	printf("TERMINE");
	sem_destroy(&a);
	sem_destroy(&b);
	pthread_mutex_destroy(&lock);

	return 0;

}

