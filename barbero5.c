#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdbool.h>
#include <semaphore.h>

int esperando=1;
bool estado_barber;  // true--> cortando    // false----> durmiendo
#define SILLAS 5// sillas sala espera
#define CANT_CLIENTE 6
sem_t a; //semaforo barbero
sem_t b; // semaforo cliente
sem_t aux;

void *cortarPelo(void* arg){
	estado_barber=true;
	printf("/Esperando: %i", esperando);
	printf("/Estado barbero: %d", estado_barber);
	printf("/Barbero: Estoy cortando el pelo\n");
	esperando--;
	sem_post(&aux);
	

}

void *sentarse_Barbero(void* arg){
	if(estado_barber==true){
				//esperar en la silla
				
				printf("/Esperando: %i", esperando);
				printf("/Estado Barbero: %d", estado_barber);
				printf("/Cliente:Leo Revista\n");
			}
	else if (estado_barber==false){
				//despertar al barber
				estado_barber=true;
				printf("/Esperando: %i", esperando);
				printf("/Estado barbero: %d", estado_barber);				
				printf("barbero: uff, emmm, grrr,,,, uuuhh\n");
				
			}
	sem_post(&b);
	sem_post(&a);


}
void* barbero(void* arg){
	sem_wait(&b);
	sem_wait(&a);
	while (true) {
		if (esperando==0){
			//dormir
			estado_barber=false;
			printf("/Esperando: %i", esperando);
			printf("/estado barbero: %d", estado_barber);
			printf("Barbero: ZZZzzzZZZzzz\n");
			sem_post(&aux);
			}
		else{
		 cortarPelo(arg);
		}

		
	}
}



void* cliente(void* arg){
	sem_wait(&aux);
	printf("Cliente: Entrando hay %i esperando",esperando);
		if(esperando < SILLAS){
			esperando++;
		sentarse_Barbero(arg);	
		}

		else{
			//no hay lugar, me voy
			printf("/Esperando: %i", esperando);
			printf("/Estado barbero: %d", estado_barber);			
			printf("cliente: chauuu nos vimoooooooooo $$$$$\n");
			sem_post(&aux);
	
	}
}
int main (void){

	sem_init(&a,0,0);
	sem_init(&b,0,0);
	sem_init(&aux,0,1);
	int i = 0;

	pthread_t p1;
	pthread_t p2[CANT_CLIENTE];

	pthread_create(&p1,NULL,barbero,NULL);
	
	for (i=0;i<CANT_CLIENTE;i++){
	pthread_create(&p2[i],NULL,cliente,NULL);
	pthread_join(p2[i],NULL);
}	
	
	pthread_join (p1, NULL);
	sem_destroy(&a);
	sem_destroy(&b);
	sem_destroy(&aux);

	return 0;

}

