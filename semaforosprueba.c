#include <stdio.h>    // para usar printf
#include <stdlib.h>         // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads



/*********************/
/*********************/
#include <semaphore.h>
sem_t a;
sem_t b;
sem_t c;
sem_t d;
sem_t aux;
sem_t e1;
sem_t e2;
sem_t e3;
sem_t f;

/*********************/
/*********************/
pthread_mutex_t mi_mutex;




void* funcion_a ()
{
    sem_wait(&a);
	pthread_mutex_lock(&mi_mutex);
	printf("Fiiiiiigaro\n");
    sem_post(&aux);
    sem_post(&b);
    sem_post(&c);
    sem_post(&d);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);

}

void* funcion_b ()
{
    sem_wait(&aux);
    sem_wait(&b);
	pthread_mutex_lock(&mi_mutex);
	printf("Figaro1\n");
 	sem_post(&aux);
	sem_post(&e1);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);
}

void* funcion_c ()
{
    sem_wait(&aux);
    sem_wait(&c);
	pthread_mutex_lock(&mi_mutex);
	printf("Figaro2\n");
 	sem_post(&aux);
	sem_post(&e2);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);
}

void* funcion_d ()
{
    sem_wait(&aux);
    sem_wait(&d);
	pthread_mutex_lock(&mi_mutex);
	printf("Figaro3\n");
 	sem_post(&aux);
	sem_post(&e3);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);
}

void* funcion_e ()
{
    sem_wait(&e1);
    sem_wait(&e2);
    sem_wait(&e3);
	pthread_mutex_lock(&mi_mutex);
	printf("Figaro Fi\n");
 	sem_post(&f);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);
}
void* funcion_f ()
{
    sem_wait(&f);
	pthread_mutex_lock(&mi_mutex);
	printf("Figaro Fa\n");
 	sem_post(&a);
	//sem_post(&b);
	//sem_post(&c);
	//sem_post(&d);
	//sem_post(&aux);
    pthread_mutex_unlock(&mi_mutex);
    pthread_exit(NULL);
}

int main ()
{
	int cant;
	printf("Introduzca repeticiones");
	scanf("%d",&cant);
      pthread_mutex_init ( &mi_mutex, NULL);

	  sem_init(&a,0,1);
	  sem_init(&b,0,0);
	  sem_init(&c,0,0);
	  sem_init(&d,0,0);
	  sem_init(&aux,0,0);
	  sem_init(&e1,0,0);
	  sem_init(&e2,0,0);
	  sem_init(&e3,0,0);
	  sem_init(&f,0,0);

	pthread_t p1; //una variable de tipo pthread_t sirve para identificar al hilo que se cree
    pthread_t p2;
	pthread_t p3;
	pthread_t p4;
	pthread_t p5;
	pthread_t p6;

	int i=0;
	while (i<cant){
    //craer y lanzar ambos threads

    pthread_create(&p1,NULL,funcion_a,NULL);
    pthread_create(&p2,NULL,funcion_b,NULL);
    pthread_create(&p3,NULL,funcion_c,NULL);
    pthread_create(&p4,NULL,funcion_d,NULL);
    pthread_create(&p5,NULL,funcion_e,NULL);
    pthread_create(&p6,NULL,funcion_f,NULL);

    //pthread_create(identificador unico, atributos del thread, funcion a ejecutar, parametros de la func a ejecutar pasado por referencia,

	pthread_join (p1,NULL);
	pthread_join (p2,NULL);
	pthread_join (p3,NULL);
	pthread_join (p4,NULL);
	pthread_join (p5,NULL);
	pthread_join (p6,NULL);
	
     i++;
}
	      pthread_exit(NULL);
sem_destroy(&a);
sem_destroy(&b);
sem_destroy(&c);
sem_destroy(&d);
sem_destroy(&aux);
sem_destroy(&e1);
sem_destroy(&e2);
sem_destroy(&e3);
sem_destroy(&f);

pthread_mutex_destroy(&mi_mutex);
}


