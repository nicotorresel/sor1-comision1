/* Programa: Tabla de multiplicar de un número */

#include <stdio.h>


int main() {

	long i, numero;

	printf( "\n 	Introduzca un numero entero: " );	
	scanf( "%ld", &numero );
	printf( "\n La tabla de multiplicar de 1 hasta el %ld es:\n", numero );

        for ( i = 1 ; i <= numero ; i++ )
            printf( "%ld * %ld = %ld \n", i, numero, i * numero );

	return 0;
}
